-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2019 at 05:38 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ride`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `numberofbooking` int(200) NOT NULL,
  `name` text NOT NULL,
  `phnumber` int(14) NOT NULL,
  `email` text NOT NULL,
  `pickuplocation` text NOT NULL,
  `droplocation` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`numberofbooking`, `name`, `phnumber`, `email`, `pickuplocation`, `droplocation`, `date`, `time`) VALUES
(5, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'pollachi', 'mcet campus,udumalai road ,pollachi', '2019-03-27', '01:59:00'),
(6, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'pollachi', 'mcet campus,udumalai road ,pollachi', '2019-03-27', '01:59:00'),
(7, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'pollachi', 'mcet campus,udumalai road ,pollachi', '2019-03-27', '01:59:00'),
(8, 'sudhakar', 2147483647, 'sudhakarkanagaraj@gmail.com', 'pollachi', 'mcet campus,udumalai road ,pollachi', '2019-03-15', '01:58:00'),
(9, 'sudhakar', 2147483647, 'sudhakarkanagaraj@gmail.com', 'pollachi', 'mcet campus,udumalai road ,pollachi', '2019-03-15', '01:58:00'),
(12, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'kjdjfhpafapfadda', 'mcet campus,udumalai road ,pollachi', '2019-03-07', '22:01:00'),
(13, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'afa', 'mcet campus,udumalai road ,pollachi', '2019-03-22', '22:01:00'),
(14, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'afa', 'mcet campus,udumalai road ,pollachi', '2019-03-22', '22:01:00'),
(15, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'afa', 'mcet campus,udumalai road ,pollachi', '2019-03-22', '22:01:00'),
(16, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'afa', 'mcet campus,udumalai road ,pollachi', '2019-03-22', '22:01:00'),
(17, 'sudhakar', 2147483647, 'sudhakarkanagaraj2004@gmail.com', 'afa', 'mcet campus,udumalai road ,pollachi', '2019-03-22', '22:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `name` text NOT NULL,
  `email` text NOT NULL,
  `subject` text NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`name`, `email`, `subject`, `text`) VALUES
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'test', 'testing'),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'test', 'test'),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'test', 'test'),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'test', 'test'),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` varchar(20) NOT NULL,
  `phnumber` int(14) NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `zip` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`name`, `email`, `password`, `phnumber`, `city`, `state`, `zip`) VALUES
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'refc', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('rgb', 'rgb', 'rgb', 453, 'tbgr', 'rtgbv', 35),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'azxc', 2147483647, 'Dindigul', 'TAMIL NADU', 76543),
('sudhakar', 'sudhakark2004@gmail.com', '123', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', '2004@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarks@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarks@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakj2004@gmail.com', 'qwe', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'asw', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'asw', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'asw', 2147483647, 'Dindigul', 'TAMIL NADU', 624613),
('sudhakar', 'sudhakarkanagaraj2004@gmail.com', 'asw', 2147483647, 'Dindigul', 'TAMIL NADU', 624613);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`numberofbooking`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `numberofbooking` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
